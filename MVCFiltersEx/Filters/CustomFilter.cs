﻿using System;
using System.Web.Mvc;
using System.Text;
using log4net;

namespace MVCFilters.Filters
{
    public class CustomFilter : ActionFilterAttribute, IExceptionFilter
    {
        private static readonly ILog _log4net = LogManager.GetLogger("log4net");

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            StringBuilder message = new StringBuilder();
            message.Append("\nController = " + filterContext.ActionDescriptor.ControllerDescriptor.ControllerName);
            message.Append(" Action = " + filterContext.ActionDescriptor.ActionName);
            message.Append(" URL = " + filterContext.HttpContext.Request.Url);
            Log(message.ToString());
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine("Action Execution Completed");
            Log(message.ToString());
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine("Result Action Executing");
            Log(message.ToString());
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine("Result Execution Completed");
            Log(message.ToString());
        }

        public void OnException(ExceptionContext filterContext)
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine(filterContext.Exception.Message);
            Log(message.ToString());
        }

        private void Log(string message)
        {
            _log4net.Info(message);
        }
    }
}