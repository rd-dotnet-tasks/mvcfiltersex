﻿using System.Web;
using System.Web.Mvc;
using MVCFilters.Filters;

namespace MVCFilters
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomFilter());
        }
    }
}
